milligramme-noir is a fork of `Milligram` intended for personal use.

It will feature:

* dark theme
* non-flat design, where appropriate

## Licence

Milligram: Designed with ♥ by [CJ Patoilo](https://cjpatoilo.com). Licensed under the [MIT License](https://cjpatoilo.mit-license.org).

Milligramme-Noir: Adjustments by [Olivier 'Reivilibre'](https://gitlab.com/reivilibre). Licensed under the same licence terms.
